import { loadImage } from './utils.js'
import constants from './constants.js'

export default class {
    constructor(view, symbols, barNum) {
        this.view = view
        this.w = this.view.barrelWidth
        this.h = this.view.canvas.height
        this.x = barNum * this.w
        this.y = 0
        this.num = barNum
        this.symbols = symbols
    }

    async draw(offsety) {
        this.view.ctx.clearRect(this.x, this.y, this.w, this.h)
        this.drawBG()
        this.drawGridLines()
        await this.drawImages(offsety)
    }

    drawBG() {
        this.view.ctx.beginPath()
        this.view.ctx.strokeStyle = 'black'
        this.view.ctx.lineWidth = '1'
        this.view.ctx.fillStyle = '#eeeeee'
        this.view.ctx.fillRect(this.x, this.y, this.w, this.h)
    }

    drawGridLines() {
        this.view.ctx.beginPath()
        this.view.ctx.fillStyle = '#aeaeae'
        for (let i = 1; i < this.view.barrelsCount; i++) {
            const linex = i * this.w
            this.view.ctx.moveTo(linex, 0)
            this.view.ctx.lineTo(linex, this.h)
            this.view.ctx.stroke()
        }
    }

    async drawImages(offsety = 0) {
        let maxJ = this.symbols.length
        for (let j = 0; j < maxJ; j++) {
            const img = await loadImage('./assets/' + this.symbols[j].id + '.png')
            let sx = 0
            let sy = 0
            let sw = constants.imageWidth
            let sh = constants.imageHeight
            let x = 10 + this.w * this.num
            let y = 10 + this.w * j - offsety
            let w = this.w - 20
            let h = this.w - 20
            this.view.ctx.drawImage(img, sx, sy, sw, sh, x, y, w, h)
            this.symbols[j].sx = sx
            this.symbols[j].sy = sy
            this.symbols[j].sw = sw
            this.symbols[j].sh = sh
            this.symbols[j].x = x
            this.symbols[j].y = y
            this.symbols[j].w = w
            this.symbols[j].h = h
        }
    }
}
