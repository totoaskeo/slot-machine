import Model from './model.js'
import View from './view.js'
import Controller from './controller.js'
import constants from './constants.js'

$(document).ready(() => {
    const canvas = document.getElementById('game-board')
    const model = new Model()
    let view
    
    window.addEventListener('resize', adaptCanvas, false)
    adaptCanvas()
    
    function adaptCanvas () {
        if (window.innerWidth < constants.canvasWidth || window.innerHeight < constants.canvasHeight) {
            canvas.width = window.innerWidth - window.innerWidth * 0.6
            canvas.height = window.innerHeight - 150
        } else {
            canvas.width = constants.canvasWidth
            canvas.height = constants.canvasHeight
        }
        view = new View(model, canvas)
        view.draw()
    }
    
    const controller = new Controller(model, view)
    const button = document.getElementById('spin')
    button.addEventListener('click', controller.handleSpin.bind(controller))
})
