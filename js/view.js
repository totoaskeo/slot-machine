import { getRandBetween, loadImage } from './utils.js'
import Barrel from './barrel.js'
import constants from './constants.js'

export default class {
    constructor (model, canvas) {
        this.ctx = canvas.getContext('2d')
        this.model = model
        this.canvas = canvas
        this.barrelsCount = model.symbols.length
        this.barrelWidth = canvas.width * 0.33
        this.focusRect = {
            x: 5,
            y: this.barrelWidth,
            w: this.canvas.width - 15,
            h: this.barrelWidth
        }
        this.barrels = []
        for (let i = 0; i < this.barrelsCount; i++)
            this.barrels.push(new Barrel(this, model.symbols[i], i))
    }

    draw () {
        for (const b of this.barrels)
            b.draw()

        this.drawFocusRect()
    }

    drawFocusRect () {
        this.ctx.beginPath()
        this.ctx.rect(this.focusRect.x, this.focusRect.y, this.focusRect.w, this.focusRect.h)
        this.ctx.lineWidth = '4'
        this.ctx.strokeStyle = '#a02b39'
        this.ctx.stroke()
    }

    animateSpin () {
        for (let i = 0; i < this.barrelsCount; i++) {
            let offsety = 0
            let dy = getRandBetween(5, 15)
            let dinterval = 10
            const pixelsToSpin = this.barrelWidth * getRandBetween(i+1, i+3)

            const interval = setInterval(() => {
                if (offsety + dy > pixelsToSpin) {
                    dy = pixelsToSpin - offsety
                    clearInterval(interval)
                    console.log(`Barrel ${i} stopped`)
                    let winLine = false
                    setTimeout(() => {
                        if (i === this.barrelsCount - 1) winLine = this.model.checkWin(this.focusRect)
                        if (winLine) {
                            this.animateWin(winLine)
                        }
                    }, 500)
                }
                offsety += dy
                dinterval -= 10
                this.barrels[i].draw(offsety)
                this.drawFocusRect()
            }, dinterval)
        }
    }

    animateWin (line) {
        console.log('Animating')
        for (let j = 0; j < 4; j++) {
            for (let i = 0; i < line.length; i++) {
                setTimeout(async () => {
                    const img = await loadImage('./assets/' + line[i].id + '.png')
                    let sx = line[i].sx + j * constants.imageWidth
                    let sy = line[i].sy
                    let sw = constants.imageWidth
                    let sh = constants.imageHeight
                    let x = line[i].x
                    let y = line[i].y
                    let w = line[i].w
                    let h = line[i].h
                    this.ctx.drawImage(img, sx, sy, sw, sh, x, y, w, h)
                }, 200 + j * 200)
            }
        }
        
    }
}
