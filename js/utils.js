export function loadImage (url) {
    return new Promise(r => {
        let i = new Image()
        i.onload = (() => r(i))
        i.src = url
    })
}

export function shuffleArray (a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1))
        ;[a[i], a[j]] = [a[j], a[i]]
    }
    return a
}

export function getRandBetween (min, max) {
    return Math.floor(Math.random() * (max - min) + min)
}
