export default class {
    constructor (model, view) {
        this.model = model
        this.view = view
    }

    handleSpin () {
        this.model.reshuffle()
        console.log('Spinning')
        this.view.animateSpin()
    }
}
