import SymbolTypes from './symbol-types.js'
import { shuffleArray } from './utils.js'

export default class {
    constructor () {
        this.symbols = []
        const availableSymbols = $.map(SymbolTypes, v => v)
        for (let i = 0; i < 3; i++) {
            let line = []
            for (let j = 0; j < 20; j++) {
                line.push({ id: availableSymbols[j % 5], y: null })
            }
            this.symbols.push(shuffleArray(line))
        }
    }

    reshuffle () {
        for (let line of this.symbols) {
            line = shuffleArray(line)
        }
    }

    checkWin (focusRect) {
        let sy = null
        for (let j = 0; j < this.symbols[0].length; j++) {
            let s = this.symbols[0][j]
            if (s.y >= focusRect.y && s.y <= focusRect.y + focusRect.h) {
                sy = s.y
            }
        }

        if (sy) {
            let line = []
            for (let i = 0; i < this.symbols.length; i++) {
                let symbol = this.symbols[i].find(s => s.y === sy)
                line.push(symbol)
            }

            console.log(line)
            if (line.some(l => l.id === line[0].id)) {
                console.log('Win')
                return line
            } else {
                return false
            }
        }
        
    }
}
