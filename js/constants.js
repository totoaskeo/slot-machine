export default {
    imageWidth: 216,
    imageHeight: 144,
    canvasWidth: 600,
    canvasHeight: 600
}
