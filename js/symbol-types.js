const SymbolTypes = Object.freeze({
    A: 'symbolA',
    B: 'symbolB',
    C: 'symbolC',
    D: 'symbolD',
    E: 'symbolE'
})

export default SymbolTypes